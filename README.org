* UwU Whats dis

Okay so basically I got tired of the fact that there is no simple PGP-based password manager for Windows. Something like gnu-pass for Linux or something.

Due to the lack of such a password manager, I had to manually open up a command line and use the ~gpg~ commandlet to decrypt my password files, then copy the password from the terminal to my clipboard.

I got so tired of this that I just made up my mind to make my own.

What I did to make this functional was that I made an [[https://www.autohotkey.com/][AutoHotkey]] script which launches ~powershell.exe~ with the path of this script as the argument whenever I press a certain key combination.

I then made a task in Task Scheduler to launch this AutoHotkey script whenever I log in. You can also just put it in the autostart folder I guess.

So yeah I guess it works or something
