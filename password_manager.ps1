﻿function Show-Notification {
    [cmdletbinding()]
    Param (
        [string]
        $ToastTitle,
        [string]
        [parameter(ValueFromPipeline)]
        $ToastText
    )

    [Windows.UI.Notifications.ToastNotificationManager, Windows.UI.Notifications, ContentType = WindowsRuntime] > $null
    $Template = [Windows.UI.Notifications.ToastNotificationManager]::GetTemplateContent([Windows.UI.Notifications.ToastTemplateType]::ToastText02)

    $RawXml = [xml] $Template.GetXml()
    ($RawXml.toast.visual.binding.text | Where-Object {$_.id -eq "1"}).AppendChild($RawXml.CreateTextNode($ToastTitle)) > $null
    ($RawXml.toast.visual.binding.text | Where-Object {$_.id -eq "2"}).AppendChild($RawXml.CreateTextNode($ToastText)) > $null

    $SerializedXml = New-Object Windows.Data.Xml.Dom.XmlDocument
    $SerializedXml.LoadXml($RawXml.OuterXml)

    $Toast = [Windows.UI.Notifications.ToastNotification]::new($SerializedXml)
    $Toast.Tag = "PowerShell"
    $Toast.Group = "PowerShell"
    $Toast.ExpirationTime = [DateTimeOffset]::Now.AddMinutes(1)

    $Notifier = [Windows.UI.Notifications.ToastNotificationManager]::CreateToastNotifier("Password Manager")
    $Notifier.Show($Toast);
}

function Show-Passwords
{
    Write-Host "------------------------------------"
    foreach ($file in $files)
    {
        Write-Output $file.Name
    }
    Write-Host "------------------------------------"
}

function Decrypt
{
    gpg --quiet --decrypt $files | Set-Clipboard
    Show-Notification -ToastTitle "$files decrypted" -ToastText "Copied password to clipboard"
}

Set-Location C:\Users\RichardMiskolczi\pass
$query = Read-Host "Enter the password you're looking for"
$files = Get-ChildItem -Path "." | Where-Object Name -Like *$query* | Sort-Object

$tmp = $files | Measure-Object
$resultCount = $tmp.Count

if ($resultCount -eq 0)
{
    Show-Notification -ToastTitle "Error" -ToastText "No such password found"
}
else
{
    while ($resultCount -gt 1)
    {
        Show-Passwords
        $substring = Read-Host "Enter a unique substring to specify which password you mean"
        $files = Write-Output $files | Where-Object Name -Like *$substring*
        $tmp = $files | Measure-Object
        $resultCount = $tmp.Count

        if ($resultCount -gt 1)
        {
            Show-Passwords
        }
    }

    Decrypt
}
